# Create your models here.
from django.db import models
from mptt.models import MPTTModel,TreeForeignKey,TreeManyToManyField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
import datetime
from django.conf import settings

class Submissions(models.Model):
    submission_id = models.AutoField(primary_key=True, db_column='submission_id')
    user_id =  models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
         db_column='user_id',
         related_name='submissions'
    )
    sub_status = models.IntegerField(blank=True, null=True)
    sub_datetime = models.DateTimeField(auto_now_add=True,editable=False,blank=True)
    question_id = models.ForeignKey('questions.Questions', on_delete=models.PROTECT,db_column='question_id',related_name='ques')
    #lang = models.CharField(max_length=10, blank=True, null=True)
    time = models.FloatField(blank=True, null=True)
    submitted_answer=models.CharField(max_length=255,blank=False,default='123')


class Bookmark(models.Model):
    question_id=models.ForeignKey(
        'questions.Questions',
        on_delete=models.PROTECT,
        db_column='question_id',
        related_name='questions'
    )
    user_id =  models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
         db_column='user_id',
         related_name='bookmark'
    )
    save_time=models.DateTimeField(auto_now_add=True,editable=False,blank=True)
    class Meta:
        unique_together =('question_id','user_id')



class Exam(MPTTModel):
    exam_id = models.AutoField(primary_key=True)
    exam_name = models.CharField(max_length=200)
    parent = TreeForeignKey(
    'self', 
    on_delete=models.CASCADE,
    null=True,
    blank=True,
    related_name='children')
    slug = models.SlugField()
    class MPTTmeta:
       level_attr = 'mptt_level'
       order_insertion_by = ['Exam_name']
    class Meta:
        managed : True
        db_table : 'exam'
        unique_together = (('parent', 'slug',))
      # verbose_name_plural = 'exams'




    def get_slug_list(self):
        try:
            ancestors = self.get_ancestors(include_self=True)
        except:
            ancestors = []
        else:
            ancestors = [ i.slug for i in ancestors]
        slugs = []
        for i in range(len(ancestors)):
            slugs.append('/'.join(ancestors[:i+1]))
        return slugs

    def __str__(self):
     return self.exam_name



class Questions(models.Model):
    question_id = models.AutoField(primary_key=True)
    question_type = models.IntegerField(default=0)
    question_text= models.TextField(blank=True)
    question_marks=models.IntegerField(default=0)
    correct_option=models.IntegerField(default=0)
    question_attempt=models.IntegerField(default=0)
    successful_attempt=models.IntegerField(default=0)
    question_answer = models.TextField(blank=True)
    difficulty_level = models.PositiveSmallIntegerField(default=0)


    #question_submissions=models.IntegerField(default=0)
    exams = TreeManyToManyField('Exam',related_name='questions')
     
    
class Option(models.Model):
   # option_id = models.AutoField(primary_key = True)
    question = models.ForeignKey(Questions,on_delete= models.CASCADE,related_name='options')
    option_description = models.TextField(blank=True)
    def __str__(self):
     return self.option_description


class Tag(models.Model):
    question = models.ForeignKey(Questions,on_delete = models.CASCADE,related_name='tags')
    tag_description = models.TextField(blank=True)


    def __str__(self):
     return self.tag_description


    class Meta: 
        managed  : True
        db_table : 'questions'
        db_table : 'option'
        db_table : 'tag'
        db_table : 'submissions'
        db_table : 'bookmark'

 
