import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map,shareReplay } from 'rxjs/operators';
import {Exam} from './../exam'
import {ExamsService} from './../services/exams.service'

@Component({
  selector: 'app-root-nav',
  templateUrl: './root-nav.component.html',
  styleUrls: ['./root-nav.component.css']
})
export class RootNavComponent implements OnInit {


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  dataSource=[];
  selectedRoute; 
  id: any;
  drawer:any;


  constructor(private router: Router ,public route: ActivatedRoute, private breakpointObserver: BreakpointObserver,private apiService: ExamsService) {}

  ngOnInit(){

    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.getUrl();

      }
    });
    // this.id=  this.route.snapshot.paramMap.get('id') ? this.route.snapshot.paramMap.get('id') : '';
    // console.log(this.id,"--<<<<---")
    console.log(window.location.href)
    if(window.location.href.includes('test/')){
      var value = window.location.href.split('/');
      this.getLeftBar(value[(value.length - 1)]);
    }
    else{
      this.getLeftBar();
    }

    
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        
        this.getUrl;
      }
    });
    }
    getLeftBar(id = ''){
    this.apiService.getExamsPage(id).
    subscribe((data : Exam[])=>{
      if(data.length > 1){
      this.dataSource=data;}
      else{
        this.dataSource=[data];
      }
      console.log(this.dataSource)
    }); 
  }
    
    getUrl(){
      this.selectedRoute = this.router.url.includes('/test');
    }
  }
  
