import { ServiceService } from './../services/service.service';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Tag } from 'src/app/exam';
import { Router, ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Question } from 'src/app/exam';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { ExamQuestionsComponent } from '../exam-questions/exam-questions.component';


@Component({
  selector: 'app-collections',  
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.css']   
})
export class CollectionsComponent implements OnInit {
  collectionForm: FormGroup;
  questions:Question[]=[];
  data:any;
  data1:any;
  @ViewChild('myTemplate',{static:true}) customTemplate: TemplateRef<any>;
  
  constructor(public service: ServiceService,public dialog: MatDialog,private fb: FormBuilder,    private router: Router) { }
  tag: Tag[]=[];
  ngOnInit() {
    

   this.collectionForm=this.fb.group({
    name: ['', Validators.required],

   });
    this.service.getCollection().
    subscribe(data=>this.data=data);
  }

  createcollect(name:String){
    this.service.createCollection(name).subscribe(
      success => {
        this.ngOnInit();

      }
    );
  }
  deleteCollection(id){
    this.service.deleteCollection(id).subscribe(
      success=>{
        this.ngOnInit();
      }
    );
    
  } 





  getcollect(id)
  {
    this.service.getQuestion(id).subscribe(data1=>this.data1=data1);
    this.openDialog();
  }
  openDialog():  void {
     
    const dialogRef = this.dialog.open(this.customTemplate,{
      width: '500px',
    });
  }


  


}
