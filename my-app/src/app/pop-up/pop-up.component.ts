import { Component, OnInit } from '@angular/core';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider,GoogleLoginProvider } from 'angularx-social-login';
import { AuthService1 } from '../auth.service';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})
export class PopUpComponent implements OnInit {
  loader:any;

  constructor(
    private authService: AuthService1,
    public dialogRef: MatDialogRef<PopUpComponent>,
    private authService1: AuthService,
    private router: Router,
    


  ) { }

  ngOnInit() {
  }


  signInWithFB(): void {
    this.loader=true;
    this.authService1.signIn(FacebookLoginProvider.PROVIDER_ID).then(x => {console.log('hello=shishir=>>',x);
    localStorage.setItem('fbtoken',x.photoUrl);
    console.log('hello dear1234>>>>>>>>',x.facebook);
  this.authService.fblogin(x.authToken).subscribe(
    success => {
      this.loader=false;
      this.dialogRef.close();
      this.router.getCurrentNavigation;
    },

  );

  });
  }
  signOut(): void {
    this.authService1.signOut();
  }
  signInWithGoogle(): void {
    this.loader=true;
    this.authService1.signIn(GoogleLoginProvider.PROVIDER_ID).then(x => {console.log('hello=====>>>>>>>>shishir=>>',x);
    this.authService.googlelogin(x.authToken).subscribe(
      success => {
        this.loader=false;
        this.dialogRef.close();
        this.router.getCurrentNavigation; 
        
      },
  
    );


    });  

  }


}
