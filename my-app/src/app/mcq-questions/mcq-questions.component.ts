import { Component, OnInit,HostListener } from '@angular/core';
import {ExamQuestionService} from '../services/exam-question.service';
import{ServiceService} from '../services/service.service'
import{LeftbarService} from './../leftbar.service';
import { Question } from 'src/app/exam';
import { PageEvent } from '@angular/material/paginator';
import {FormControl, Validators} from '@angular/forms';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
interface qFilter {
  id: number;
  name: string;
}
@Component({
  selector: 'app-mcq-questions',
  templateUrl: './mcq-questions.component.html',
  styleUrls: ['./mcq-questions.component.css']
})
export class McqQuestionsComponent implements OnInit {
  loader:any;
  loader1:any;
  data :any;
  
  url='https://api.doubtfix.com/test/';
  url1='https://api.doubtfix.com/sloved/';
  questions:Question[]=[];
  pageEvent: PageEvent;
  questionUrl1= 'https://api.doubtfix.com/';
  // questionUrl= 'https://api.doubtfix.com/questions';
  questionUrl= 'https://api.doubtfix.com/questions';
  next: string;
  ur1:number;
  previous: string;
  filterControl = new FormControl('');
  pageNo=0;
  totalCount=0;
  selectFormControl = new FormControl('');
  idFilters: qFilter[] = [
    {id: 1, name: 'Basic'},
    {id: 2, name: 'Easy'},
    {id: 3, name: 'Medium'},
    {id: 4, name: 'Hard'},
  ];

  typeFilters: qFilter[] = [
    {id: 1, name: 'MCQ'},
    {id: 2, name: 'True/False'},
    {id: 3, name: 'Paragraph'},
    {id: 4, name: 'Fill in'},
  ];
  analytics:string;
  url2: string;
  data4:any;
  flag:any;
  constructor(private EqService: ExamQuestionService,     
    private leftbar:LeftbarService,
    private submit:ServiceService,
    private scrollDispatcher: ScrollDispatcher) {
      //this.scrollDispatcher.scrolled().subscribe(x => this.onScroll({}));
     }

  
  ngOnInit() {
    this.leftbar.currentMessage.subscribe(url=>{
      console.log("shishirChaubey123==>",url);
      this.url='https://api.doubtfix.com/test/';
      this.analytics='https://api.doubtfix.com/analytics/' + url;
      this.url1='https://api.doubtfix.com/sloved/';
      this.questions=[]; 
      this.next='';
      this.previous='';
      this.url+=url;
      this.url1+=url;
      console.log(this.url1);
      this.fectchFilter();
      this.flag=false;
      //this.setQuestions(this.url)
    });
  }

  // fetchData(id,url){
  //   this.loader=true;
  //   this.EqService.getExamQuestionPage(id,url).
  //   subscribe(data=>{this.data=data;
  //                    this.loader=false;
     
      
  
    
  //   });
  // }
  
  setQuestions(url, type = null) {
    this.loader=true;
    this.loader1=true;
    this.EqService.getExamQuestionPage(url).subscribe(data => {
      //this.questions.push(data.results);
      this.loader=false;
      this.loader1=false;
      if (type == "questionType"){
        this.commonFunction();
      }     
        else if(type == "filter")
        this.questions = data.results;
      else 
        this.questions = this.questions.concat(data.results);

      this.totalCount=data.count;
      
        // set the components next property here from the response
        this.next = data.next;
      

      
        // set the components previous property here from the response
        this.previous = data.previous;
      
    });
  }
  fetchNext() {
    if(this.next !== null && this.next !== undefined && this.next !=='')
      this.setQuestions(this.next);
    else
      return;
  }

  // function fetches the previous paginated items by using the url in the previous property
  fetchPrevious() {
    this.setQuestions(this.previous);
  }


  fectchFilter(){
    console.log(this.filterControl);
    console.log(this.selectFormControl.value.id);
    let id = this.filterControl.value.id !== undefined? 'difficulty_level=' +  this.filterControl.value.id:'';
    let type = this.selectFormControl.value.id !== undefined? 'question_type=' + this.selectFormControl.value.id:'';
    
    let endpoint='';
    if (id !== '' && type !== ''){
      endpoint = id+'&'+type;
    }
    else
      endpoint = id+type;
      
    this.setQuestions(this.url+  '/?'+ endpoint, "filter")
  }

  nextPrevious(page: number){
    
    if(this.pageNo > page){
      this.fetchPrevious();
    }
    else{
      this.fetchNext();
    }
    this.pageNo=page; 
  }
// @HostListener("document:touchmove", ['$event'])
//@HostListener("window:scroll", ['$event'])
onScroll(): void {
//   console.log('dad');
// if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
  this.fetchNext();
    // }
}
getFreshScore(index: number) {
  if(index==1)
  {  
    this.url2=this.url;  
    this.url=this.url1;
    this.commonFunction();
    // this.setQuestions(this.url,"questionType");
    // console.log('jhkjgfjkegjkegc==>>',this.url);
  }
  if(index==0)
  {
    this.url=this.url2; 
    // this.questions=[];
    // this.setQuestions(this.url);
    this.commonFunction();
  }

}

commonFunction(){
  this.questions=[]; 
  this.next='';
  this.previous='';
  this.fectchFilter();

}


getanalytics(){
  this.submit.getanalytics(this.analytics).subscribe(data4=>{
    this.flag=true;
    this.data4=data4;

  });


} 

}









      


