import { Component, OnInit ,Inject} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService1 } from '../auth.service';
import { EventEmitter,Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog'
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider,GoogleLoginProvider } from 'angularx-social-login';
import{LeftbarService} from '../leftbar.service';
import { from } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Output() closeModalEvent = new EventEmitter<boolean>();
  form: FormGroup;
  public loginInvalid: boolean;
  private formSubmitAttempt: boolean;
  private returnUrl: string;
  loader:any;
  user: SocialUser;
  constructor(
    private fb:FormBuilder,
    private route:ActivatedRoute,
    private router: Router,
    private authService: AuthService1,
    public dialogRef: MatDialogRef<LoginComponent>,
    private authService1: AuthService,
    private userdetail:LeftbarService,
      ) {}
  

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    // this.authService1.authState.subscribe((user) => {
    //   this.user = user;
    //   console.log(user);
    // });

  }
  login() {
    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      this.loader=true;
    this.authService.login(this.form.get('username').value,this.form.get('password').value).subscribe(
      
      success => {
        this.dialogRef.close();
        console.log("this is close");
        this.loader=false;
        this.router.navigate(['questions']); 

        
       this.userdetail.UserDeatail(true);
      },
      error =>{ this.loginInvalid = true;
        this.loader=false;
      }
    );
    
    }
    else {
      this.formSubmitAttempt = true;
    }
  }



  // signInWithFB(): void {
  //   this.authService1.signIn(FacebookLoginProvider.PROVIDER_ID).then(x => {console.log('hello=shishir=>>',x);
  //   localStorage.setItem('fbtoken',x.photoUrl);
  //   console.log('hello dear1234>>>>>>>>',x.facebook);
  // this.authService.fblogin(x.authToken).subscribe(

  // )});
  // }
  // signOut(): void {
  //   this.authService1.signOut();
  // }
  // signInWithGoogle(): void {
  //   this.authService1.signIn(GoogleLoginProvider.PROVIDER_ID).then(x => {console.log('hello=====>>>>>>>>shishir=>>',x);
  //   this.authService.googlelogin(x.authToken).subscribe()});  

  // }













  


} 
