import { Component, OnInit } from '@angular/core';
import{ServiceService} from '../services/service.service'
import { ActivatedRoute,Router } from '@angular/router';


@Component({
  selector: 'app-ques-details',
  templateUrl: './ques-details.component.html',
  styleUrls: ['./ques-details.component.css']
})
export class QuesDetailsComponent implements OnInit {
  data={};
  id:any;


  constructor(private hello:ServiceService,public route: ActivatedRoute) { }

  ngOnInit(): void {
  this.id=  this.route.snapshot.paramMap.get('id');
  this.hello.questionDetails(this.id)
  .subscribe(data=> this.data=data)

  console.log('hello shishir==>>>',this.id)
  }

}
