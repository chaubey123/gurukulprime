import{LeftbarService} from './../leftbar.service';
import {Exam} from './../exam'
import {Component, HostBinding, Input, OnInit} from '@angular/core';

import {Router} from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.css'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({transform: 'rotate(0deg)'})),
      state('expanded', style({transform: 'rotate(180deg)'})),
      transition('expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ])
  ]
})
export class LeftNavComponent implements OnInit {
  expanded: boolean = false;
  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @Input() item: Exam;
  @Input() depth: number;
  selectedRoute; 

  constructor(public router: Router,public Leftbar:LeftbarService) {
    if (this.depth === undefined) {
      this.depth = 0;
    }
  }

  ngOnInit() {
    this.getUrl();
    
  }
  getUrl(){
    this.selectedRoute = this.router.url;
    console.log(this.router)
  }




  onItemSelected(item: Exam) {    
    if (item.subexams && item.subexams.length) {
      this.expanded = !this.expanded;
    }
  }

fetch(id){
  this.Leftbar.changeMessage(id);
}
}