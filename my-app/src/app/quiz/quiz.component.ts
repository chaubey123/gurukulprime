import { Component, OnInit } from '@angular/core';
// import {ServiceService} from './../../services/service.service';
import { ActivatedRoute,Router } from '@angular/router';
import {ServiceService} from './../services/service.service';
import  {Question} from '../exam';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
  items = [{'question_id':1,'option_id':123}];
  currentItem = 'Television';
  optionData: Question[] = [];
  id:any;

  constructor(public route: ActivatedRoute, private router: Router,
    private quiz:ServiceService,) { }

  ngOnInit() { 
    this.id=  this.route.snapshot.paramMap.get('id');
    this.getquiz(this.id) 

    
  }
  data3:any; 
  data4:{}       
  data9:{}
  data5:{} 
  data:any
  countDown:any
getquiz(id){
  this.quiz.getquiz(id).subscribe(data3=>{
    this.data3=data3;
    
    this.data4=this.data3.questions;
    this.data5=this.data3.id;
    console.log(this.data4,"this is quizs")
  })
  // this.openDialog();
} 





addItem(newItem: any) {
  console.log(newItem,"<<<<this isjg")
  let flag=true;
  this.items.forEach(ele=>{
    if(ele.question_id == newItem.question_id){
      flag = false
      ele.option_id = newItem.option_id
    }
  });
  if(flag){
    this.items.push(newItem);
  }
  
  this.optionData = newItem.optionData
  console.log(this.items)
} 

onsubmitt(id){
  console.log(id,"this quiz id"); 
  this.quiz.submittQuiz(id,this.items).subscribe(data=>{
    data.forEach(element => {
      this.optionData[element.question_id].status=element.sub_status;     
      this.optionData[element.question_id].disableAnswer=true;
      this.optionData[element.question_id].correctId=element.correct_id;      
    });
      
  })

} 



}


function countController($scope){
  $scope.countDown = 10;    
  var timer = setInterval(function(){$scope.countDown--; console.log($scope.countDown)},1000);  
}​​


