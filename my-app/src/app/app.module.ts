import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component'; // <-- NgModel lives here
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { QuestionsComponent } from './questions/questions/questions.component';
import { LeftNavComponent } from './left-nav/left-nav.component';
import { MaterialModule } from './material';
import { RootNavComponent } from './root-nav/root-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { ExamQuestionsComponent } from './exam-questions/exam-questions.component';
import { SignupComponent } from './signup/signup.component';
import { AuthService1, AuthInterceptor,AuthGuard } from './auth.service';
import { PopUpComponent } from './pop-up/pop-up.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileComponentDialog } from './profile/profile.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { CreateQuestionComponent } from './create-question/create-question.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { McqQuestionsComponent } from './mcq-questions/mcq-questions.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { DragDropDirective } from './directive/drag-drop.directive';
import { FooterComponent } from './footer/footer.component';
import { FlexLayoutModule } from "@angular/flex-layout";
import { DisqusModule } from 'ngx-disqus';
import { QuesDetailsComponent } from './ques-details/ques-details.component';
import { SocialLoginModule,AuthServiceConfig } from 'angularx-social-login';
import { FacebookLoginProvider,GoogleLoginProvider} from 'angularx-social-login';
import { ScriptLoaderService } from 'angular-google-charts';
<<<<<<< HEAD
import {FillinComponent} from './fillin/fillin.component';
import {InfiniteScrollComponent} from './infinite-scroll/intinite-scroll'

=======
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import {InfiniteScrollComponent} from './infinite-scroll/intinite-scroll';
import { FillinComponent } from './fillin/fillin.component';
import { CollectionsComponent } from './collections/collections.component';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import { SolvedQuestionComponent } from './solved-question/solved-question.component';
import { TableComponent } from './table/table.component';
import { HomeComponent } from './home/home.component';
import { QuizComponent } from './quiz/quiz.component';
import { QuizQuestionsComponent } from './quiz-questions/quiz-questions.component';
import { NewComponentComponent } from './new-component/new-component.component';
>>>>>>> efdaa51a5e25cab46b89931e54df56f949001287
const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('85548885356-85fbr4ej0hfj09fn52gmcbojgfpjtt83.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('572865190269075')
  },
  // {
  //   id: LinkedInLoginProvider.PROVIDER_ID,
  //   provider: new LinkedInLoginProvider("78iqy5cu2e1fgr")
  // }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    DashboardComponent,
    QuestionsComponent,
    LeftNavComponent,
    RootNavComponent,
    ExamQuestionsComponent,
    SignupComponent,
    PopUpComponent,
    ProfileComponent,
    ProfileComponentDialog,
    CreateQuestionComponent,
    McqQuestionsComponent,
    BookmarksComponent,
    DragDropDirective,
    FooterComponent,
    QuesDetailsComponent,
<<<<<<< HEAD
    FillinComponent,
    InfiniteScrollComponent
=======
    InfiniteScrollComponent,
    FillinComponent,
    CollectionsComponent,
    SolvedQuestionComponent,
    TableComponent,
    HomeComponent,
    QuizComponent,
    QuizQuestionsComponent,
    NewComponentComponent
>>>>>>> efdaa51a5e25cab46b89931e54df56f949001287
  ],
  imports: [
    FormsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot([]),
    LayoutModule,
    FlexLayoutModule,
    GoogleChartsModule.forRoot(),
    SocialLoginModule,
    DisqusModule.forRoot('gurukulprime'),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: false }),

  ],
  entryComponents: [
    ExamQuestionsComponent,ProfileComponentDialog
  ],
  providers: [AuthService1,AuthGuard,ScriptLoaderService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: MatDialogRef,
      useValue: {}
    },
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

