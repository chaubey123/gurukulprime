import { Component, OnInit } from '@angular/core';
import {  FormBuilder, FormGroup, FormArray, Validators, FormGroupName } from '@angular/forms';
import { throwIfEmpty } from 'rxjs/operators';
import { ServiceService } from '../services/service.service';
import{ExamsService} from '../services/exams.service';
import {Exam} from './../exam'
import {MatSnackBar} from '@angular/material/snack-bar';


interface Mark {
  value: number;
  viewValue: string;
}

interface Type {
  value: number;
  viewValue: string;
}


interface Level {
  value: number;
  viewValue: string;
}
interface Correct {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css'],
})
export class DashboardComponent implements OnInit  {
  marks: Mark[] = [
    {value: 1, viewValue: '1'},
    {value: 2, viewValue: '2'},
    {value: 4, viewValue: '4'},
    {value: 8 ,viewValue: '8'}
  ];
  types: Type[] = [
    {value: 1, viewValue: 'MCQ'},
    {value: 2, viewValue: 'True/false'},
    {value: 3, viewValue: 'Fill in '},
    {value: 4, viewValue: 'Paragraph'},
    
  ];
  levels: Level[] = [
    {value: 1, viewValue: 'Basic'},
    {value: 2, viewValue: 'Easy'},
    {value: 3, viewValue: 'Medium'},
    {value: 4, viewValue: 'Hard'}
  ];

  corrects: Correct[] = [
    {value: 1, viewValue: '1'},
    {value: 2, viewValue: '2'},
    {value: 3, viewValue: '3'},
    {value: 4, viewValue: '4'},
    {value: 5, viewValue: '5'}
  ];
  submitted = false;
  leagueForm: FormGroup;
  dataSource={};
  loader:any;
  create:any;
  question: Object;

  itemList = [];
  selectedItems = [];
  settings = {};
  
  constructor(private CreateService : ServiceService,private fb: FormBuilder,private _snackBar: MatSnackBar,private Exams:ExamsService) {}

  logToConsole(object: any) {
    console.log(object);
  }

  ngOnInit() {
    this.leagueForm = this.fb.group({
      teams: this.fb.array([this.teams])
    });

 
  }
 

  get teams(): FormGroup {
    return this.fb.group({
      question_text:['',Validators.required],
      question_mark:['',Validators.required],
      //exam_id:['',Validators.required],
      question_type:['',Validators.required],
      difficulty_level:['',Validators.required],
      correct_option:['',Validators.required],
      question_answer:['',Validators.required],
      options:this.fb.array([this.options]),
      tags: this.fb.array([this.tags]),
      exam_id:this.fb.array([this.fb.control('')])
    });
  }
  get options():FormGroup {
    return this.fb.group({
      option_description:['',Validators.required]
    })
  }

  onSubmit() {
    
    
    this.submitted = true;
    if (this.leagueForm.invalid) {
        return;
    }
    this.CreateQuestion(this.leagueForm.value.teams);
    console.log(this.leagueForm.value.teams);
    
  }


  get tags(): FormGroup {
    return this.fb.group({
      tag_description: ""
    });
  }

  get hello():FormGroup{
    return this.fb.group({
      exam_id: ""
    });
  }

  get exam_id() {
    //return this.leagueForm.get("teams").get('exam_id') as FormArray;
    console.log(this.teams.get('exam_id'));
    return this.teams.get('exam_id') as FormArray
  }




  addTeam() {
    (this.leagueForm.get("teams") as FormArray).push(this.teams);
  }

  /*deleteTeam(index) {
    (this.leagueForm.get("teams") as FormArray).removeAt(index);
  }*/

  addTag(team) {
    team.get("tags").push(this.tags);
  }
  addExamhello(team){
    team.get("exam_id").push(this.exam_id)
  }

  addExam(team) {
    team.get('exam_id').push(this.fb.control(''));
  }

  deleteExam(team,index) {
    team.get("exam_id").removeAt(index);
    }


  addOption(team){
    team.get("options").push(this.options)
  }
  deleteOption(team,index) {
  team.get("options").removeAt(index);
  }

  deleteTag(team, index) {
    team.get("tags").removeAt(index);
  }

  CreateQuestion(request){
    this.loader=true;
    this.create=false;
    for(let i=0;i<request.length;i++){
     
    this.CreateService.postQuestions(request[i]).
    subscribe(data=>{console.log("hello---neeraj  >>",data);
        this.create=true;
        this.question=data;
        this.loader=false;
        this.openSnackBar("Question Created");

      
    },);
  }
}
openSnackBar(message: string) {
  this._snackBar.open(message, "", {
    duration: 2000,
  });
}  
}

























