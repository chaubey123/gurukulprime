
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LeftbarService {

  private messageSource = new BehaviorSubject('1');
  currentMessage = this.messageSource.asObservable();
  private userDetail = new BehaviorSubject(false);
  currentUser = this.messageSource.asObservable();


  changeMessage(message: string) {
    this.messageSource.next(message);
  }
  UserDeatail(flag){
    this.userDetail.next(flag);

  }

}