import { Injectable , Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap, shareReplay } from 'rxjs/operators';
import * as jwtDecode from 'jwt-decode';
import * as moment from 'moment';

import { environment } from '../environments/environment';

@Injectable()
export class AuthService1 {
  private apiRoot = 'https://api.doubtfix.com/auth/';
  private apiFb='https://api.doubtfix.com/fblog';
  private apiGoogle='https://api.doubtfix.com/googlelog';
  private apiRoot1 = 'https://api.doubtfix.com/auth/';
  private apiFb1='https://api.doubtfix.com/fblog';
  private apiGoogle1='https://api.doubtfix.com/googlelog';
  constructor(private http: HttpClient) { }

  private setSession(authResult) {
    console.log('authResult:::',authResult);
    const token = authResult.token;
    console.log(token);
    console.log("hello dear=====>>>",token);
    
    const payload = <JWTPayload> jwtDecode(token);
    console.log('====>>>>',payload);
    
    const expiresAt = moment.unix(payload.exp);

    localStorage.setItem('token', authResult.token);
    localStorage.setItem('user', JSON.stringify(authResult.user));
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
  }
  get token(): string {
    return localStorage.getItem('token');
  }
  login(username: string, password: string) {
    return this.http.post(
      this.apiRoot.concat('login/'),
      { username, password },
    ).pipe(
      tap(response => this.setSession(response)),
      shareReplay(),
    );
  }
  fblogin(access_token:string){
    return this.http.post(
      this.apiFb,
      {access_token},
      ).pipe(
        tap(response => this.setSession(response)),
        shareReplay(),
      );

  }

  googlelogin(access_token:string){
    return this.http.post(
      this.apiGoogle,
      {access_token},
      ).pipe(
        tap(response => this.setSession(response)),
        shareReplay(),
      );

  }


  signup(username: string, email: string, password1: string, password2: string) {
    // TODO: implement signup
    return this.http.post(
      this.apiRoot.concat('signup/'),
      { username, email, password1, password2 }
    ).pipe(
      tap(response => this.setSession(response)),
      shareReplay(),
    );
  }
  logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
  }
  refreshToken() {
    if (moment().isBetween(this.getExpiration().subtract(1, 'days'), this.getExpiration())) {
      return this.http.post(
        this.apiRoot.concat('refresh-token/'),
        { token: this.token }
      ).pipe(
        tap(response => this.setSession(response)),
        shareReplay(),
      ).subscribe();
    }
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);

    return moment(expiresAt);
  }

  isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');
    console.log('---hrlloooiohhh-->>>',token);

    if (token) {
      console.log('--shishi--->>>',token);
      const cloned = req.clone({
        headers: req.headers.set('Authorization', 'JWT '.concat(token))
        
      });

      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService1, private router: Router) { }

  canActivate() {
    if (this.authService.isLoggedIn()) {
      this.authService.refreshToken();

      return true;
    } else {
      this.authService.logout();
      this.router.navigate(['login']);

      return false;
    }
  }
}

interface JWTPayload {
  user_id: number;
  orig_iat:number;
  email: string;
  exp: number;
  username:string;

}

