import { Component, OnInit } from '@angular/core';
import {AuthService1} from '../auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import{ Router,ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  form: FormGroup;
  public loginInvalid: boolean;
  private formSubmitAttempt: boolean;
  private returnUrl: string;
  error:any;
  loader:any;

  constructor(
    private fb:FormBuilder,
    private route:ActivatedRoute,
    private authService: AuthService1,
    private router : Router, 
    public dialogRef: MatDialogRef<SignupComponent>,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password1:['',Validators.required],
      password2: ['', Validators.required],
      email:['',Validators.email]
    });
  }
  signup(){
    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    
    if (this.form.valid) {
    this.loader=true;
    this.authService.signup(this.form.get('username').value,this.form.get('email').value,this.form.get('password1').value,this.form.get('password2').value).subscribe(
      success => {
        this.loader=false;
        this.router.navigate(['questions']);
        this.dialogRef.close();
    },
      error => {this.loginInvalid = true;
        this.loader=false;
      });
  }

  else {
    this.formSubmitAttempt = true;
    
  }

}
}