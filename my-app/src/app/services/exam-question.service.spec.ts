import { TestBed } from '@angular/core/testing';

import { ExamQuestionService } from './exam-question.service';

describe('ExamQuestionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExamQuestionService = TestBed.get(ExamQuestionService);
    expect(service).toBeTruthy();
  });
});
