import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from  '@angular/common/http';
import {Exam} from './../exam';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ExamsService {
  constructor(private  httpClient:  HttpClient) { }
  API_URL  = 'https://api.doubtfix.com/examsonly';
  API_URL1  = 'https://api.doubtfix.com/examsonly';
  
  getExamsPage(id = ''): Observable<Exam[]>{
    return  this.httpClient.get<Exam[]>(`${this.API_URL1}/${id}`);
  }
}
