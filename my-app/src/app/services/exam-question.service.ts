import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from  '@angular/common/http';
import { Observable } from 'rxjs';
import {Exam} from './../exam';

@Injectable({
  providedIn: 'root'
})
export class ExamQuestionService {
  constructor(private  httpClient:  HttpClient) {}
  getExamQuestionPage(url:String): Observable<any>  {
    let API_URL  =  url;
    let API_URL1  = 'https://api.doubtfix.com/test/'+ url;
    
    return  this.httpClient.get< any>(`${API_URL}`);
  }



  getQuestion(endpoint){
    // const endpoint=this.root1 +'Userappcollections/' + id
    return this.httpClient.get(endpoint);
  }

}


