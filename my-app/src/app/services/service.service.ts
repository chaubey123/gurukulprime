import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from  '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  root2='https://api.doubtfix.com/'
  root1='https://api.doubtfix.com/'

  Question_Details=this.root1 + 'Questionadd/'
  Get_Submitted= this.root1 + 'checksub';
  Upload_pic=this.root1 + 'profiles/'
  Bookmark_Ques= this.root1 + 'bookmarks/'
  Post_Ques=this.root1 +'questions/'
  Submit_URL= this.root1 +'test1/'
  Subjective_Sub=this.root1 +'subject/'
  Profile_URL=this.root1 +'users'
  Get_Bookmark=this.root1 +'Mybooks/'
  API_URL  = this.root1 +'questions';
  constructor(private  httpClient:  HttpClient) {}
  getFirstPage(API_URL: string):Observable<any>{
    return  this.httpClient.get<any>(API_URL);
  }

  getProfilePage(){
    return this.httpClient.get(`${this.Profile_URL}`);

  }






  postSubmission(request){
    return this.httpClient.post(`${this.Submit_URL}`,{ 
      "question_id":request.question_id,
      "option_id":request.option_id
    });
  }

  subSubmission(request){
    return this.httpClient.post(`${this.Subjective_Sub}`,{ 
      "question_id":request.question_id,
      "submitted_answer":request.submitted_answer,
    });
  }



  postQuestions(request){
    return this.httpClient.post(`${this.Post_Ques}`,{ 
      "exam_id":request.exam_id,
      "difficulty_level":request.difficulty_level,
      "question_type":request.question_type,
      "question_text":request.question_text,
      "question_marks":request.question_mark,
      "correct_option":request.correct_option,
      "question_answer":request.question_answer,
      "options":request.options,
      // "tags":request.tags,
    //   "qutag" :[{
    //     "name": "hskjshfkshfk"
    // },
    // {
    //     "name": "jgjjg"
    // }]
     });
  }
  saveBookmark(request){
    return this.httpClient.post(`${this.Bookmark_Ques}`,{
      "question_id":request.question_id
    });
  }
  getBookmark(){
    return this.httpClient.get(`${this.Get_Bookmark}`);
  }
  deleteBookmark(id){
    const deletebook=this.getBookmark + id;
    return this.httpClient.delete(deletebook)

  }
  questionDetails(id){
    let endpoint=this.Question_Details + id
    return this.httpClient.get(endpoint);
  }
  updateQuestion(id,item){
    let endpoint=this.Question_Details + id
    return this.httpClient.put(endpoint,item);
  }
  
  postFile(fileToUpload,user_id){
    
    const endpoint = this.Upload_pic + user_id + '/pic/'
    
    return this.httpClient
      .put(endpoint, fileToUpload);
}
 updateProfile(item,user_id){
    
    const endpoint = this.Upload_pic + user_id + '/'
    
    return this.httpClient
      .put(endpoint, item);
}

Submitted(request){
  return this.httpClient.post(`${this.Get_Submitted}`,{ 
    "question_id":request.question_id,
    "user_id":request.user_id
  });
}
getCollection(){
  const endpoint=this.root1 +'createcollection'
  return this.httpClient.get(endpoint);

}
deleteCollection(id){
  const endpoint=this.root1 +'Userappcollections/' + id
  return this.httpClient.delete(endpoint);

}
getQuestion(id){
  const endpoint=this.root1 +'Userappcollections/' + id
  return this.httpClient.get(endpoint);
}
gettag(){
  const endpoint=this.root1 + 'tags'
  return this.httpClient.get(endpoint);
}
createCollection(name){
  const endpopint=this.root1 +'createcollection'
  return this.httpClient.post(endpopint,{
    "name":name.name

  })
}


getanalytics(id){

  return this.httpClient.get(id);  
}




getquiz(url){
  const endpoint=this.root1 + 'quiz/' + url
  return this.httpClient.get(endpoint);
}
submittQuiz(id,items): any {

const endpoint=this.root1 + 'postquiz' 
return this.httpClient.post(endpoint,{
  "id":id,
  "questions":items

})
}

}



