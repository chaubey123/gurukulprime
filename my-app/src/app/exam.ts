export interface Exam {    
        exam_id: Number;
        exam_name: String;
        subexams?:Exam[];     
}

export interface Question{
        optionId:Number;
        correctId:Number;
        status:Number;
        disableAnswer:Boolean;
      }
export interface Tag{
        name: String;
        questions?:Question[];
      }