import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, from } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { PopUpComponent } from './../pop-up/pop-up.component';
import { AuthService1 } from '../auth.service';
import * as jwtDecode from 'jwt-decode';
import{LeftbarService} from '../leftbar.service'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user:any={};
  data:any;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,public dialog: MatDialog,private authservice:AuthService1 ,private leftbar:LeftbarService) { 

  }

  ngOnInit() {
    this.leftbar.currentUser.subscribe(res=>{
      if(res){
        this.guser();
        // if (localStorage.getItem('token')){
          // this.user= jwtDecode(localStorage.getItem('token'));
          // this.data=this.user.username;
          
          // console.log("shishirchaubey===>>>",this.user.username);
          // this.data.pic=     localStorage.getItem('fbtoken') ;
          // console.log("hellopopopo==>>",this.data.pic)
     // }
    }
  })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PopUpComponent, {
      width: '400px',
    });
    
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.guser();
    });
  }

  logout() {
    this.authservice.logout();
  }

  isLoggedIn(){
    return this.authservice.isLoggedIn() ;
 
      
  }
  
   guser(){
     console.log('skc');
     this.user=JSON.parse(localStorage.getItem('user'));
    console.log("shishirchaubey===>>>",this.user);


}
}
