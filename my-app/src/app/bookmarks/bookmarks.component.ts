import { Component, OnInit } from '@angular/core';
import { ServiceService } from './../services/service.service';
@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {
  data : any;
  constructor( public service: ServiceService) { }

  ngOnInit() {
    this.service.getBookmark().
    subscribe(data=>this.data=data);
  }

}
