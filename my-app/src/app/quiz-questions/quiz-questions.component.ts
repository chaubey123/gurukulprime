
import { Component,Inject, Optional,OnInit ,Input ,OnChanges,ViewChild, TemplateRef} from '@angular/core';
import{ServiceService} from '../services/service.service'
import { ActivatedRoute,Router } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import { isEmpty } from 'rxjs/operators';
import  {Question} from '../exam';
import { AuthService1 } from '../auth.service';
import * as jwtDecode from 'jwt-decode';
import { request } from 'http';
import { PopUpComponent } from './../pop-up/pop-up.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-quiz-questions',
  templateUrl: './quiz-questions.component.html',
  styleUrls: ['./quiz-questions.component.css']
})
export class QuizQuestionsComponent implements OnChanges {
  // "quiz":"ng g c quiz-questions --module app",
  @Output() newItemEvent = new EventEmitter<{}>();
  @Input() item = '';
 



  quizOptionData: any;

  constructor(public route: ActivatedRoute, private router: Router,
    private Submit:ServiceService,
    private authservice:AuthService1 ,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<QuizQuestionsComponent>,
    
   // @Inject(MAT_DIALOG_DATA) data),
    private _snackBar: MatSnackBar,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log('data passed in is:', this.data);
      
      
     }
  
  @Input() question:any;
  @Input() i:Number;
  @Input() optionData: Question[] = [];
  data1=[];
  // public optionData: Question[] = [];
  optionsValue=['A','B','C','D','E','F'];
  loader=[];
  color=[];
  user :any;
  show = false;
  @ViewChild('myTemplate',{static:true}) customTemplate: TemplateRef<any>;
  
  ngOnChanges() {
    if (this.isLoggedIn()){
     this.user= jwtDecode(localStorage.getItem('token'));
     console.log('hello SHishir  chaubey  ===>>',this.user)
    let request={ 
      "question_id":this.question.question_id,
      "user_id": this.user.user_id}

    this.Submit.Submitted(request).subscribe((data:any)=>{
      //this.optionData[this.question.question_id].disableAnswer=true
      this.optionData[this.question.question_id]={
        optionId:0,
        correctId:0,
        status:data.sub == 0 ? 0:1,
        disableAnswer:data.sub == 0 ? false:true,
      },
      this.color[this.question.question_id]=data.book==0 ?false:true;

      console.log(data)

    })
  }
    }


  options(id,q_id){
    console.log(q_id,"kjgfh")
    //if(this.optionData[q_id] != )
    this.optionData[q_id]={
      optionId:id,
      correctId:id,
      status:1,
      disableAnswer:false
    };
    this.newItemEvent.emit({'question_id':q_id,'option_id':this.optionData[q_id].optionId,optionData:this.optionData });
    // this.quizFun(id);
  }

  quizFun(id){
    this.quizOptionData.push(id);
  }

  quizSubmit(){
    this.quizOptionData.forEach(element => {
      this.fun(element);
    });
  }

  fun(id){
    
    
    if(this.optionData[id] !== undefined&&this.optionData[id].optionId&&localStorage.getItem('token')){ 
      this.loader[id]=true;
     let request={ 
      "question_id":id,
      "option_id": this.optionData[id].optionId

    }; 
     this.Submit.postSubmission(request).
     subscribe( (data:any)=>{
      this.loader[id]=false;
      this.optionData[id].status=data.sub_status;     
      this.optionData[id].disableAnswer=true;
      this.optionData[id].correctId=data.correct_id;      
    });
      
  }
  else{
    this.openSnackBar("Login to Submit Or select Option");
    //this.openDialog();
    
  }
   }

   openSnackBar(message: string) {
    this._snackBar.open(message, "", {
      duration: 2000,
    });
  }  
  // addNewItem(value: string) {
  //   this.newItemEvent.emit(value);
  // }

    
  onBookmark(id){
    let request={
      "question_id":id
    }
    this.Submit.saveBookmark(request).
    subscribe(data=>{
      this.color[id]=true;
    });
  }

  getBookmarks(){
    this.Submit.getBookmark()
    .subscribe(data1=>this.data1) 
  }
  

  getQues(){
     
  }

  isLoggedIn(){
    return this.authservice.isLoggedIn() ;
 
      
  }
  gettag(){
    this.Submit.getCollection().subscribe(data3=>{
    this.data3=data3;
    console.log("helloshishir==???",data3);
  }
    )
    this.openDialog();
  }
  data3:any;
  openDialog(): void {
     
    const dialogRef = this.dialog.open(this.customTemplate,{
      width: '500px',
    });
  }

 
  addcollection(){
    
  }

}

  