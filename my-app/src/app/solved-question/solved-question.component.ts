import { Component, OnInit, Input } from "@angular/core";
import { ServiceService } from "../services/service.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { isEmpty } from "rxjs/operators";
import { Question } from "../exam";
import * as jwtDecode from "jwt-decode";
import { request } from "http";
import { PopUpComponent } from "./../pop-up/pop-up.component";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";

@Component({
  selector: "app-solved-question",
  templateUrl: "./solved-question.component.html",
  styleUrls: ["./solved-question.component.css"],
})
export class SolvedQuestionComponent implements OnInit {
  constructor(
    public route: ActivatedRoute,
    private router: Router,
    private Submit: ServiceService,
    public dialog: MatDialog
  ) {}

  @Input() question: any;
  @Input() i: Number;
  data1 = [];
  public optionData: Question[] = [];
  optionsValue = ["A", "B", "C", "D", "E", "F"];
  loader = [];
  color = [];
  user: any;
  show = false;

  ngOnInit() {}

  options(id, q_id) {
    //if(this.optionData[q_id] != )
    this.optionData[q_id] = {
      optionId: id,
      correctId: id,
      status: 1,
      disableAnswer: false,
    };
  }

  onBookmark(id) {
    let request = {
      question_id: id,
    };
    this.Submit.saveBookmark(request).subscribe((data) => {
      this.color[id] = true;
    });
  }

  getQues() {}
}
