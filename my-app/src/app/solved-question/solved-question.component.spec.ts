import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolvedQuestionComponent } from './solved-question.component';

describe('SolvedQuestionComponent', () => {
  let component: SolvedQuestionComponent;
  let fixture: ComponentFixture<SolvedQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolvedQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolvedQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
