import { Component, OnInit } from '@angular/core';
import {  FormBuilder, FormGroup, FormArray, Validators, FormGroupName } from '@angular/forms';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  leagueForm: FormGroup;
  temp=200;
  hello=0;
  counter=1;
  countervalue=[];
  x=0;
  k=0;
  l=0;
  y=0;
  color=[];
  ids0 = [1, 26, 32, 36];
  ids1 = [1, 2, 20, 33];
  ids2 = [1, 2, 3,21,25];
  ids3 = [2, 3, 4, 26,35];
  ids4 = [3, 4, 5, 19,21];
  ids5 = [4, 5, 6, 10,24];
  ids6 = [5,6,7,8,28,29];
  ids7 = [6, 7,8,28,29];
  ids8 = [7,8,9, 23, 30];
  ids9 = [8,9,10,22, 31];
  ids10 = [5, 9, 10,11,23]; 
  ids11 = [10,11,12,30,36];
  ids12 = [11,12,13,28,35];
  ids13 = [12,13,14,27,36];
  ids14 = [13,14,15,20,31];
  ids15 = [14,15,16,19,32];
  ids16 = [15,16,17,24,33];
  ids17 = [16,17,18,25,34];
  ids18 = [17,18,19,22,29];
  ids19 = [4,15,18,19,20];
  ids20 = [1,14,19,20,21];
  ids21 = [2,4,20,21,22];
  ids22 = [9,18,21,22,23];
  ids23 = [8,10,22,23,24];
  ids24 = [5,16,23,24,25];
  ids25 = [2,17,24,25,26];
  ids26 = [3,25,26,27,32];
  ids27 = [6,13,26,27,28];
  ids28 = [7,12,27,28,29];
  ids29 = [8,11,29,30,31];
  ids30 = [8,11,29,30,31];
  ids31 = [9,14,30,31,32];
  ids32 = [15,26,30,31,32];
  ids33 = [1,16,32,33,34];
  ids34 = [6,17,33,34,35];
  ids35 = [3,12,34,35,36];
  ids36 = [11,13,35,36,1];


  

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.leagueForm = this.fb.group({
      teams: this.fb.array([this.teams])
    });
  }




  get exam_id() {
    //return this.leagueForm.get("teams").get('exam_id') as FormArray;
    console.log(this.teams.get('exam_id'));
    return this.teams.get('exam_id') as FormArray
  }
  get teams(): FormGroup {
    return this.fb.group({
      exam_id:this.fb.array([this.fb.control('')])
    });
  }



  addExamhello(team){
    team.get("exam_id").push(this.exam_id)
  }

  addExam(team) {
    while (this.temp != 0) {
    team.get('exam_id').push(this.fb.control(''));
    this.temp=this.temp - 1;
    }
  }

  deleteExam(team,index) {
    team.get("exam_id").removeAt(index);
    }


    onSubmit() {
    
   
    }
    getFirst(team,index) {
      return team.get("exam_id").get(index);
    }


    onKey(searchValue: any,i): void {
      console.log("hhhhhhh",i)
      console.log("hello previouse value==>>",this.y)
      let c=this.y
      if(searchValue<=36)
      {
        
        if (c==1)
        {
          
           let a = this.ids1.filter(function (r) {
             console.log("ttttt---->>",r)
             return r == searchValue
           })
          // let a = this.ids1.includes(c)
          //console.log('a:::::==>>>', a)
          if (a.length)
          {
           this.k=1;
           this.l=0;
           
           this.color[i]=1;
           this.color[i-1]=1;
           this.countervalue[i]=this.counter;
           this.countervalue[i-1]=this.counter;
            console.log("element found");
            console.log(this.y);
          }
          else
          {
            this.l=this.l + 1;
            this.color[i]=0;
            let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
           
            //this.counter=this.counter + 1;
          }
        }


        else if(c==2){
          //console.log("searchvalue==>>>",c);
          let a = this.ids2.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
           console.log("element found");
           console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

              if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }

             
           }
           
           //this.counter=this.counter + 1;
         }

        }


        else if(c==3){
          let a = this.ids3.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
           console.log("element found");
           console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
              else if(this.counter )
              this.counter=1
           }
            
           //this.counter=this.counter + 1;
         }

        }


        else if(c==4){
          let a = this.ids4.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
           console.log("element found");
           console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
              
           }
           //this.counter=this.counter + 1;
         }

        }


        else if(c==5){
          let a = this.ids5.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
             
       
           console.log("element found");
           console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
           //this.counter=this.counter + 1;
         }

        }


        else if(c==6){
          let a = this.ids6.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
           //this.counter=this.counter + 1;
         }

        }


        else if(c==7){
          let a = this.ids7.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)

         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
           //this.counter=this.counter + 1;
         }
        }


        else if(c==8){
          let a = this.ids8.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }

        }


        else if(c==9){
          let a = this.ids9.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }                            
             
           }
         }
        }


        else if(c==10){
          let a = this.ids10.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==11){
          console.log("Shishir")
          let a = this.ids11.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==12){
          let a = this.ids12.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==13){
          let a = this.ids13.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }

        }

        else if(c==14){
          console.log("hello tab==>>>",c)
          let a = this.ids14.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }

        }

        else if(c==15){
          let a = this.ids15.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }

        else if(c==16){
          let a = this.ids16.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }

        }

        else if(c==18){
          let a = this.ids18.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }

        else if(c==19){
          let a = this.ids19.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }

        else if(c==20){
          let a = this.ids20.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }

        else if(c==21){
          let a = this.ids21.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }

        else if(c==22){
          let a = this.ids22.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==23){
          let a = this.ids23.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==24){
          let a = this.ids24.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==25){
          let a = this.ids25.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }



        else if(c==25){
          let a = this.ids25.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }



        else if(c==26){
          let a = this.ids26.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==27){
          let a = this.ids27.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==28){
          let a = this.ids28.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==28){
          let a = this.ids28.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }



        else if(c==29){
          let a = this.ids29.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
          
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==30){
          let a = this.ids30.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==31){
          let a = this.ids31.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }

        else if(c==32){
          let a = this.ids32.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==33){
          let a = this.ids33.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==34){
          let a = this.ids17.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
          
              
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==35){
          let a = this.ids35.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
       
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
         }
        }


        else if(c==36){
          let a = this.ids36.filter(function (r) {
            return r == searchValue
          })
         // let a = this.ids1.includes(c)
         console.log('a:::::==>>>', a)
         if (a.length)
         {
          this.k=1;
          this.l=0;
              
       
          
          this.color[i]=1;
          this.color[i-1]=1;
          this.countervalue[i]=this.counter;
          this.countervalue[i-1]=this.counter;
          let z=0;
          for(let j=i-2;j>i-1 && i>4;j--){
            if(this.color[j])
              z++;
          }
          if(z==1)
            this.counter =1
          
          
          console.log("element found");
          console.log(this.y);
         }
         else
         {
           this.l=this.l + 1;
           this.color[i]=0;
           let count=0;
           for(let j=0;j < i ;j++){
            if(this.color[j]==0)
              count++;

                if(count == 4)
              {
              this.counter=this.counter + 1 ;
              count=0;
              }
             
           }
           
         }
        }

       //console.log(searchValue/2);
        console.log("shishir");
        let y=searchValue;
        console.log("hello i amm ==>>",y);
        this.x=searchValue;
        this.y=searchValue;
        console.log(this.x);

      }
    }
  

}
