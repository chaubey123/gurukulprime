import { Component, OnInit, HostListener ,ViewChild, TemplateRef} from '@angular/core';
import {ServiceService} from './../../services/service.service';
import { Question } from 'src/app/exam';
import {FormControl, Validators} from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import * as e from 'express';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';


interface qFilter {
  id: number;
  name: string;
}
@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {



  opened: boolean;
  @ViewChild('myTemplate',{static:true}) customTemplate: TemplateRef<any>;
  questions:Question[]=[];
  pageEvent: PageEvent;
  questionUrl1= 'https://api.doubtfix.com/questions';
  questionUrl= 'https://api.doubtfix.com/questions';
  next: string;
  events:any;
  previous: string;
  filterControl = new FormControl('');
  pageNo=0;
  totalCount=0;
  selectFormControl = new FormControl('');
  idFilters: qFilter[] = [
    {id: 1, name: 'Level 1'},
    {id: 2, name: 'Level 2'},
    {id: 3, name: 'Level 3'},
    {id: 4, name: 'Level 4'},
  ];

  typeFilters: qFilter[] = [
    {id: 1, name: 'MCQ'},
    {id: 2, name: 'True/False'},
    {id: 3, name: 'Paragraph'},
    {id: 4, name: 'Others'},
  ];
  constructor(private apiService:ServiceService,public dialog: MatDialog) { }
 data={};
 data3={};
 
  ngOnInit() {
    this.setQuestions(this.questionUrl);
    //this.gettag();
   
  }
  setQuestions(url: string, type = null) {
    this.apiService.getFirstPage(url).subscribe(data => {
      //this.questions.push(data.results);
      if(type == "filter")
        this.questions = data.results;
      else 
        this.questions = this.questions.concat(data.results);

      this.totalCount=data.count;
      
        // set the components next property here from the response
        this.next = data.next;
      

      
        // set the components previous property here from the response
        this.previous = data.previous;
      
    });
  }
  fetchNext() {
    if(this.next !== null && this.next !== undefined && this.next !=='')
      this.setQuestions(this.next);
    else
      return;
  }

  // function fetches the previous paginated items by using the url in the previous property
  fetchPrevious() {
    this.setQuestions(this.previous);
  }


  fectchFilter(){
    console.log(this.filterControl);
    console.log(this.selectFormControl.value.id);
    let id = this.filterControl.value.id !== undefined? 'difficulty_level=' +  this.filterControl.value.id:'';
    let type = this.selectFormControl.value.id !== undefined? 'question_type=' + this.selectFormControl.value.id:'';
    
    let endpoint='';
    if (id !== '' && type !== ''){
      endpoint = id+'&'+type; 
    }
    else
      endpoint = id+type;
      
    this.setQuestions(this.questionUrl+ '/?'+ endpoint, "filter")
  }

  nextPrevious(page: number){
    
    if(this.pageNo > page){
      this.fetchPrevious();
    }
    else{
      this.fetchNext();
    }
    this.pageNo=page;
  }

 
onScroll(){
  this.fetchNext();
   
}


gettag(){
  this.apiService.gettag().subscribe(data3=>this.data3=data3)
  this.openDialog();
}

openDialog(): void {
     
  const dialogRef = this.dialog.open(this.customTemplate,{
    width: '500px',
  });
}
}
