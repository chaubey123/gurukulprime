import { Component, OnChanges,Input } from '@angular/core';
import{ServiceService} from '../services/service.service'
import { ActivatedRoute,Router } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import { isEmpty } from 'rxjs/operators';
import  {Question} from '../exam'
import * as jwtDecode from 'jwt-decode';
import { request } from 'http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PopUpComponent } from './../pop-up/pop-up.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
@Component({
  selector: 'app-fillin',
  templateUrl: './fillin.component.html',
  styleUrls: ['./fillin.component.css']
})
export class FillinComponent implements  OnChanges {
  constructor(public route: ActivatedRoute, private router: Router,
    private Submit:ServiceService,
    private fb:FormBuilder,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar) { }
  
  @Input() question:any;
  @Input() i:Number;
  data1=[];
  public optionData: Question[] = [];
  optionsValue=['A','B','C','D','E','F'];
  loader=[];
  color=[];
  user :any;
  form: FormGroup;
  submitted_answer: string = "doubtFix";    
  ngOnChanges() {
    if (localStorage.getItem('token')){
     this.user= jwtDecode(localStorage.getItem('token'));
     let request={ 
      "question_id":this.question.question_id,
      "user_id": this.user.user_id}

    this.Submit.Submitted(request).subscribe((data:any)=>{
      this.optionData[this.question.question_id]={
        optionId:0,
        correctId:0,
        status:data.sub == 0 ? 0:1,
        disableAnswer:data.sub == 0 ? false:true,
      },
      this.color[this.question.question_id]=data.book==0 ?false:true;

      console.log(data)

    })
 
  }
    }


  options(id,q_id){
    //if(this.optionData[q_id] != )
    this.optionData[q_id]={
      optionId:id,
      correctId:id,
      status:1,
      disableAnswer:false
    };
  }

  fun(id){
    
    
    if(localStorage.getItem('token')){ 
      this.loader[id]=true;
      let request={ 
      "question_id":id,
      "submitted_answer":this.submitted_answer

    }; 
    
     this.Submit.subSubmission(request).
     subscribe( (data:any)=>{
      this.loader[id]=false;
      this.optionData[id].status=data.sub_status;     
      this.optionData[id].disableAnswer=true;      
    });
      
  }
  else{
    //this.openSnackBar("Login to Submit Or select Option");
    this.openDialog();
    
  }
   }

   openSnackBar(message: string) {
    this._snackBar.open(message, "", {
      duration: 2000,
    });
  }  


  openDialog(): void {
    const dialogRef = this.dialog.open(PopUpComponent, {
      width: '400px',
    });
  }
    
  onBookmark(id){
    let request={
      "question_id":id
    }
    this.Submit.saveBookmark(request).
    subscribe(data=>{
      this.color[id]=true;
    });
  }

  getBookmarks(){
    this.Submit.getBookmark()
    .subscribe(data1=>this.data1) 
  }
  

  getQues(){
     
  }

}
  
