import { Component, NgModule } from '@angular/core';
import{HeaderComponent} from './header/header.component';
import { RouterModule, Routes } from '@angular/router';
import{LoginComponent} from './login/login.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import{QuestionsComponent} from './questions/questions/questions.component';
import { from } from 'rxjs';
import { ExamQuestionsComponent } from './exam-questions/exam-questions.component';
import{SignupComponent} from './signup/signup.component';
import {PopUpComponent} from './pop-up/pop-up.component';
import { AuthGuard } from './auth.service';
import{ProfileComponent} from './profile/profile.component';
import{CreateQuestionComponent} from './create-question/create-question.component'
import { McqQuestionsComponent } from './mcq-questions/mcq-questions.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { QuesDetailsComponent } from './ques-details/ques-details.component';
import {CollectionsComponent}from './collections/collections.component';
import{TableComponent} from './table/table.component'
import { HomeComponent } from './home/home.component';
import { QuizComponent } from './quiz/quiz.component';
import { NewComponentComponent } from './new-component/new-component.component';
const routes: Routes = [
  { path: 'signup', component: SignupComponent },
  {path: 'header',component:HeaderComponent},
  {path: 'questions',
  component:QuestionsComponent
},
  { path: 'login', component: PopUpComponent },

  { path: '', 
  redirectTo: '/home', 
  pathMatch: 'full' ,
},
  {path:'profile',component:ProfileComponent},
  { path: 'bookmarks', component: BookmarksComponent },
   { path: 'testing/:id', component: CreateQuestionComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'test/:id', component: McqQuestionsComponent },
  { path: 'test', component: McqQuestionsComponent },
  { path: 'detail/:id', component: QuesDetailsComponent },
  {path: 'questions/:id',component:QuestionsComponent},
  { path: 'collections', component:CollectionsComponent},
  {path:'prabhatgoyal',component:TableComponent},
  {path:'quiz/:id',component:QuizComponent},
  {path:'quiz123',component:NewComponentComponent},
  {
    path:'home',
    component:HomeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }