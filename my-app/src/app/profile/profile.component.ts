import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ServiceService } from '../services/service.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ScriptLoaderService } from 'angular-google-charts';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatDialog} from '@angular/material/dialog';


@Component({
  selector: 'profile-component-dialog',
  templateUrl: 'profile-component-dialog.html',
})
export class ProfileComponentDialog   {


  profileForm:any;
  updateProductData:any;
  
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  //private readonly chartPackage = ;
  files: File;
  file = "User Image"; 
  show = false;
  title = 'Your Submissions';
  type = 'PieChart';
  data1 = [
    ['Correct Attempt', 50],
    ['Wrong Attempt', 50]
  ];
  columnNames = ['Browser', 'Percentage'];
  options = {
    is3D: true,
  backgroundColor: '#fafafa'

  };
  width = "100%";
  height = "100%";
  dynamicResize="true";
  bookMarkData: any;
  profileForm: FormGroup;
  title1 = 'Your Correct Submissions';
  type1 = 'PieChart';
  data2 = [
    ['Level Zero', 50],
    ['Level One', 50],
    ['Level Two', 50],
    ['Level Three', 50],
    ['Level Four', 50],
  ];
  columnNames1 = ['Browser', 'Percentage'];

  constructor(public dialog: MatDialog,public profile: ServiceService,private loaderService: ScriptLoaderService,private fb: FormBuilder) {
    this.profileForm = this.fb.group({
      username: ['', Validators.required],
      bio: ['', Validators.required],
      email: ['', Validators.required],
      marks: ['', Validators.required],
      birth_date:['', Validators.required],
        });
   }
  data = {};
  ngOnInit() {
    this.profile.getProfilePage().
      subscribe((data:any) => {
        this.data = data;
        this.profileForm.controls['username'].setValue(this.data[0].username);
        this.profileForm.controls['bio'].setValue(this.data[0].profile.bio);
        this.profileForm.controls['marks'].setValue(this.data[0].profile.marks);
        this.profileForm.controls['email'].setValue(this.data[0].email);
        this.profileForm.controls['birth_date'].setValue(this.data[0].profile.birth_date);
        
        console.log("shishir===>>>shishir",data);

        this.loaderService.loadChartPackages(this.type);
        this.googleChart(data[0].profile);
       this.googleChart1(data[0].profile);
        if (this.data[0].profile.profile_photo != undefined && this.data[0].profile.profile_photo != null && this.data[0].profile.profile_photo != "")
          this.files = this.data[0].profile.profile_photo;
        else
          this.files = null;
      });
  }
  googleChart(user) {
    let wrongP = (user.total_attempt - user.successful_attempt);
    let correctP = (user.successful_attempt);
    this.data1 = [
      ['Correct Attempt', correctP],
      ['Wrong Attempt', wrongP]
    ];

    this.show = true;
  }
  googleChart1(user) {
    let levelZero = (user.level_zero);
    let levelOne = (user.level_one);
    let levelTwo = (user.level_two);
    let levelThree = (user.level_three);
    let levelFour = (user.level_four);
    this.data2 = [
      ['Level Zero', levelZero],
      ['Level One', levelOne],
      ['Level Two', levelTwo],
      ['Level Three', levelThree],
      ['Level Four', levelFour]
    ];

    this.show = true;


  }

  uploadFile(event) {
    this.files = event[0];
    this.file = this.files.name;
    

    this.uploadFileToActivity();     
  }


deleteAttachment(index) {
  this.files = null;

}

uploadFileToActivity() {
  const formData = new FormData();
  formData.append('profile_photo', this.files);

  this.profile.postFile(formData, this.data[0].profile.user).subscribe((data:any) => {
    this.data[0].profile.profile_photo = data.profile_photo;
    this.files =this.data[0].profile.profile_photo;
  }, error => {
    console.log(error);
  });
}

openDialog() {
  const dialogRef = this.dialog.open(ProfileComponentDialog,{
    
  });
  dialogRef.afterClosed().subscribe(result => {
    console.log(`Dialog result: ${result}`);
  });
}


updateProductData(values){
const productData = new FormData();
productData.append('user',this.data[0].profile.user);
productData.append('bio', values.bio);

this.profile.updateProfile(productData,this.data[0].profile.user).subscribe(result=>{
})
}
}

