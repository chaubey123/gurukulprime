import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../services/service.service';
import {  FormBuilder, FormGroup, FormArray, Validators, Form, FormControl } from '@angular/forms';
import {ExamsService} from '../services/exams.service';
import { ActivatedRoute,Router } from '@angular/router';


@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {
  submitted = false;
  leagueForm: FormGroup;
  dataSource={};
  data={};
  id:any;
  reactiveForm = new FormGroup({
    question_text: new FormControl(),
    question_marks: new FormControl(),
    question_type: new FormControl(),
    difficulty_level: new FormControl(),
    correct_option:new FormControl(),
    question_answer:new FormControl(),
    options:new FormGroup({
      option_description: new FormControl(),
    })

  })
  constructor(private CreateService : ServiceService,private fb: FormBuilder,private Exams:ExamsService,public route: ActivatedRoute) {}

  controls: FormArray;
  ngOnInit():void {
    this.id=  this.route.snapshot.paramMap.get('id');
    this.CreateService.questionDetails(this.id).subscribe(data=> {this.data=data;
    console.log('heelo???===>>>',data);
    console.log("hello froms==>>>",this.reactiveForm);
    this.setDefault(data);})
  }
  setDefault(data){
    let contact = {
      question_text:data.question_text,
      question_marks:data.question_marks,
      question_type:data.question_type,
      correct_option:data.correct_option,
      question_answer:data.question_answer,
      difficulty_level:data.difficulty_level,
      options:{
        option_description:data.options[0].option_description,

      }


    };
      this.reactiveForm.setValue(contact);
      
  }
  onSubmit() {
    console.log(this.reactiveForm.value);
  }

  setValue() {
 
    let contact = {
      question_text: "Rahul"};
  this.reactiveForm.setValue(contact);

}}